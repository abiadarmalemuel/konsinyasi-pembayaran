﻿Imports DevExpress.XtraSplashScreen
Imports System.Globalization
Imports DevExpress.XtraReports
Public Class Retur_penerimaan
    Dim dts As New DataTable
    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click

        'melihat data id penerimaan
        If LookUpEdit1.EditValue.ToString = String.Empty Then
            MessageBox.Show("Pilih Supplier terlebih dahulu")
        ElseIf DateEdit1.EditValue = Nothing Then
            MessageBox.Show("Pilih tanggal terlebih dahulu")
        Else
            info = "retur"
            retur_id.Show()
        End If



    End Sub

    Private Sub Retur_penerimaan_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim newCulture As CultureInfo = DirectCast(CultureInfo.CurrentCulture.Clone(), CultureInfo)
        newCulture.DateTimeFormat = (New CultureInfo("id-ID")).DateTimeFormat
        newCulture.NumberFormat = (New CultureInfo("id-ID")).NumberFormat
        System.Threading.Thread.CurrentThread.CurrentCulture = newCulture
        System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture

        Dim dt As DataTable = New DataTable
        dt = dataquery("select idSupplier 'ID',namaSupplier 'Supplier',Catatan, Telepon  from m_supplier where isKonsinyasi=1 ", conapppurchasing)
        LookUpEdit1.Properties.DataSource = dt
        LookUpEdit1.Properties.DisplayMember = "Supplier"
        LookUpEdit1.Properties.ValueMember = "ID"
        LookUpEdit1.Properties.PopulateColumns()
        LookUpEdit1.Properties.Columns("ID").Visible = False



    End Sub



    Private Sub idpenerimaan_TextChanged(sender As Object, e As EventArgs) Handles idpenerimaan.TextChanged
        'isi data grid control barang sesuai dengan id 

        If idpenerimaan.Text <> "-" Then
            SplashScreenManager.ShowForm(GetType(WaitForm1))


            dts = dataquery("select namaBrg 'Barang',d.qty 'Qty Diterima', dr.qty 'Qty Retur',m.idBrg 'id',mr.createdate,dr.idBrg
from  db_laritta.mbarang m, dtpenerimaan d,appbs.mretur mr, appbs.dtretur dr
where d.id=" & getIdofIdNum(idpenerimaan.Text) & " and d.idBarang=m.idBrg and mr.idRetur=dr.idRetur and dr.idBrg=m.idBrg 
and mr.createdate='" & CDate(DateEdit1.EditValue).ToString("yyyy-MM-dd") & "';", conappkonsinyasi)
            If dts.Rows.Count = 0 Then
                Dim x As DialogResult = MessageBox.Show("Tidak ada barang sisa pada tanggal " & DateEdit1.EditValue & " yang dapat diretur", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
            Dim dt As DataTable = New DataTable


            GridControl1.DataSource = dts





            SplashScreenManager.CloseForm()
        End If


    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles cancel.Click
        LookUpEdit1.EditValue = Nothing
        idpenerimaan.Text = "-"
        faktursupp.Text = "-"
        catatan.Text = ""
        TextEdit1.Text = ""
        GridControl1.DataSource = Nothing
        DateEdit1.EditValue = Nothing
    End Sub

    Private Sub SimpleButton2_Click_1(sender As Object, e As EventArgs) Handles SimpleButton2.Click

        Dim simpan As Boolean = False
        Dim qty As Boolean = True
        If LookUpEdit1.EditValue <> Nothing And TextEdit1.EditValue <> Nothing And DateEdit1.EditValue <> Nothing And dts.Rows.Count <> 0 Then
            'For index = 0 To GridView1.RowCount - 1
            '    If GridView1.GetRowCellValue(index, GridView1.Columns(2)) < GridView1.GetRowCellValue(index, GridView1.Columns(3)) Then
            '        qty = False
            '    End If
            'Next
            simpan = True
        End If
        If simpan = True And qty = True Then
            SplashScreenManager.ShowForm(GetType(WaitForm1))

            'insert master retur
            insertall("insert into mretur(idPenerimaan,idStaff,idSupplier,catatan,penerimaRetur,createDate,void) values(" & getIdofIdNum(idpenerimaan.Text) & "," & idstaff & "," & LookUpEdit1.EditValue & ",'" & catatan.Text & "','" & TextEdit1.Text & "','" & CDate(DateEdit1.EditValue).ToString("yyyy-MM-dd") & "',0) ", conappkonsinyasi)

            'update master penerimaan (isRetur)
            insertall("update mpenerimaan SET isRetur = 1 WHERE id=" & getIdofIdNum(idpenerimaan.Text) & "; ", conappkonsinyasi)

            'insert detail retur
            Dim num As Integer = dataquery("select id from mretur order by id desc limit 1", conappkonsinyasi).Rows(0).Item("id").ToString
            Dim rw As Integer = GridView1.RowCount - 1
            For index = 0 To rw
                insertall("insert into dtretur values(" & num & "," & dts.Rows(index).Item("id") & "," & repCSpr(GridView1.GetRowCellValue(index, GridView1.Columns(3))) & ");", conappkonsinyasi)
            Next

            'sum price for journal 
            Dim sumall As Decimal = dataquery("select d.id,idBarang ,harga, qty, sum(harga*qty) 'total'
            from mretur m,dtretur d,mbarang_konsinyasi b
            where m.id=d.id and d.idBarang=b.idBrg and m.id=" & num & " GROUP BY d.id;", conappkonsinyasi).Rows(0).Item("total")

            'insert ke journal 
            insertall("CALL insert_journal(now(),20," & num & ",0,'retur konsinyasi',null," & idstaff & ",now(),'1//476//0//" & repCSpr(sumall) & "|2//85//1//" & repCSpr(sumall) & "')", conappfinance)

            Dim rs As DialogResult = MessageBox.Show("Data Tersimpan", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)



            Dim x As New returReport
            x.Parameters("penkonsi").Value = getIdofIdNum(idpenerimaan.Text)
            x.Parameters("penerimaan").Value = idpenerimaan.Text
            x.Parameters("Supplier").Value = LookUpEdit1.Text
            x.Parameters("staff").Value = Module1.staff
            SplashScreenManager.CloseForm()
            Using prnt As UI.ReportPrintTool = New UI.ReportPrintTool(x)
                SplashScreenManager.ShowForm(GetType(WaitForm1))
                prnt.ShowPreviewDialog()
                SplashScreenManager.CloseForm()
            End Using

            LookUpEdit1.EditValue = Nothing
            idpenerimaan.Text = "-"
            faktursupp.Text = "-"
            catatan.Text = ""
            TextEdit1.Text = ""
            DateEdit1.EditValue = Nothing
            GridControl1.DataSource = Nothing
        ElseIf simpan = False Then
            Dim x As DialogResult = MessageBox.Show("Data belum lengkap", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        ElseIf qty = False Then
            Dim y As DialogResult = MessageBox.Show("Qty retur tidak boleh melebihi qty diterima", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If


    End Sub


End Class