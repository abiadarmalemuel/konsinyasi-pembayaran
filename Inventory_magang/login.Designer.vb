﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class login
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(login))
        Me.PictureEdit1 = New DevExpress.XtraEditors.PictureEdit()
        Me.TextEditname = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditpass = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.closebut = New DevExpress.XtraEditors.SimpleButton()
        Me.loginbut = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditname.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditpass.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PictureEdit1
        '
        Me.PictureEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureEdit1.EditValue = CType(resources.GetObject("PictureEdit1.EditValue"), Object)
        Me.PictureEdit1.Location = New System.Drawing.Point(0, 0)
        Me.PictureEdit1.Name = "PictureEdit1"
        Me.PictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit1.Size = New System.Drawing.Size(448, 198)
        Me.PictureEdit1.TabIndex = 0
        '
        'TextEditname
        '
        Me.TextEditname.Location = New System.Drawing.Point(154, 60)
        Me.TextEditname.Name = "TextEditname"
        Me.TextEditname.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.TextEditname.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.TextEditname.Properties.Appearance.Options.UseBackColor = True
        Me.TextEditname.Properties.Appearance.Options.UseForeColor = True
        Me.TextEditname.Size = New System.Drawing.Size(155, 20)
        Me.TextEditname.TabIndex = 1
        '
        'TextEditpass
        '
        Me.TextEditpass.Location = New System.Drawing.Point(154, 98)
        Me.TextEditpass.Name = "TextEditpass"
        Me.TextEditpass.Properties.Appearance.BackColor = System.Drawing.SystemColors.Control
        Me.TextEditpass.Properties.Appearance.ForeColor = System.Drawing.Color.Black
        Me.TextEditpass.Properties.Appearance.Options.UseBackColor = True
        Me.TextEditpass.Properties.Appearance.Options.UseForeColor = True
        Me.TextEditpass.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TextEditpass.Size = New System.Drawing.Size(155, 20)
        Me.TextEditpass.TabIndex = 2
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.BackColor = System.Drawing.Color.White
        Me.LabelControl1.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl1.Location = New System.Drawing.Point(154, 46)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(126, 13)
        Me.LabelControl1.TabIndex = 3
        Me.LabelControl1.Text = "Username/Password salah"
        '
        'closebut
        '
        Me.closebut.Location = New System.Drawing.Point(234, 124)
        Me.closebut.Name = "closebut"
        Me.closebut.Size = New System.Drawing.Size(75, 23)
        Me.closebut.TabIndex = 7
        Me.closebut.Text = "Tutup"
        '
        'loginbut
        '
        Me.loginbut.Location = New System.Drawing.Point(154, 124)
        Me.loginbut.Name = "loginbut"
        Me.loginbut.Size = New System.Drawing.Size(75, 23)
        Me.loginbut.TabIndex = 6
        Me.loginbut.Text = "Login"
        '
        'login
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(448, 198)
        Me.ControlBox = False
        Me.Controls.Add(Me.closebut)
        Me.Controls.Add(Me.loginbut)
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.TextEditpass)
        Me.Controls.Add(Me.TextEditname)
        Me.Controls.Add(Me.PictureEdit1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "login"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditname.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditpass.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PictureEdit1 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents TextEditname As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditpass As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents closebut As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents loginbut As DevExpress.XtraEditors.SimpleButton
End Class
