﻿Imports DevExpress.XtraSplashScreen
Imports System.Globalization
Imports DevExpress.XtraReports
Public Class Pembayaran

    Private Sub Pembayaran_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim newCulture As CultureInfo = DirectCast(CultureInfo.CurrentCulture.Clone(), CultureInfo)
        newCulture.DateTimeFormat = (New CultureInfo("id-ID")).DateTimeFormat
        newCulture.NumberFormat = (New CultureInfo("id-ID")).NumberFormat
        System.Threading.Thread.CurrentThread.CurrentCulture = newCulture
        System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture

        'data supplier
        Dim dt As DataTable = New DataTable
        dt = dataquery("select idSupplier 'ID',namaSupplier 'Supplier',Catatan, Telepon  from m_supplier where isKonsinyasi=1 ", conapppurchasing)
        LookUpEdit1.Properties.DataSource = dt
        LookUpEdit1.Properties.DisplayMember = "Supplier"
        LookUpEdit1.Properties.ValueMember = "ID"
        LookUpEdit1.Properties.PopulateColumns()
        LookUpEdit1.Properties.Columns("ID").Visible = False

        'isi data coa
        Dim dt1 As DataTable = New DataTable
        dt1 = dataquery("select idCOA 'idCoa', accName 'accName' from m_coa where rank='5' and accParent>='1110500' and accParent<='1110600';", conappfinance)
        lueBank.Properties.DataSource = dt1
        lueBank.Properties.ValueMember = "idCoa"
        lueBank.Properties.DisplayMember = "accName"
        lueBank.Properties.PopulateColumns()
        lueBank.Properties.Columns("idCoa").Visible = False


    End Sub

    Private Sub frompembayaran(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        'melihat data id penerimaan
        If LookUpEdit1.EditValue.ToString = String.Empty Then
            MessageBox.Show("Pilih Supplier terlebih dahulu")
        Else
            info = "pembayaran"
            invoice_id.Show()
        End If
    End Sub


    Private Sub SimpleButton3_Click(sender As Object, e As EventArgs) Handles SimpleButton3.Click

        Dim simpan As Boolean = False
        If bank.Text <> "" And norek.Text <> "" And an.Text <> "" And lueBank.EditValue <> Nothing Then
            simpan = True
        End If
        If simpan = True Then
            SplashScreenManager.ShowForm(GetType(WaitForm1))


            'insert master pembayaran
            insertall("insert into mpembayaran(idStaff,idCoa,bankTujuan,nominal,createDate,void) values(" & idstaff & "," & lueBank.EditValue & ",concat('" & bank.Text & "',' ','" & norek.Text & "',' ','" & an.Text & "'),'" & repCSpr(nominal.EditValue) & "',now(),0) ", conappkonsinyasi)

            'insert to dtpembayaran untuk detail id invoice yang dibayar
            Dim nm As Integer = Val(dataquery("select id from mpembayaran order by id desc limit 1;", conappkonsinyasi).Rows(0).Item("id").ToString)
            For index = 0 To GridView1.RowCount - 1
                Dim ids As Integer = getIdofIdNum1(GridView1.GetRowCellValue(index, GridView1.Columns(0)))
                insertall("insert into dtpembayaran(id,idInvoice) values(" & nm & ", " & ids & ") ", conappkonsinyasi)
            Next

            'update invoice isPaid
            For index = 0 To GridView1.RowCount - 1
                Dim id As Integer = getIdofIdNum1(GridView1.GetRowCellValue(index, GridView1.Columns(0)))
                insertall("update invoice SET isPaid = 1 where id=" & id & " ", conappkonsinyasi)
            Next


            'insert ke journal 
            insertall("CALL insert_journal(now(),17," & nm & ",0,'pembayaran konsinyasi',null," & idstaff & ",now(),'1//476//0//" & repCSpr(nominal.EditValue) & "|2//" & lueBank.EditValue & "//1//" & repCSpr(nominal.EditValue) & "')", conappfinance)
            SplashScreenManager.CloseForm()
            Dim rs As DialogResult = MessageBox.Show("Data Tersimpan", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            LookUpEdit1.EditValue = Nothing




            bank.Text = ""
            norek.Text = ""
            an.Text = ""
            lueBank.EditValue = Nothing
            GridControl1.DataSource = Nothing
        Else
            Dim x As DialogResult = MessageBox.Show("Data belum lengkap", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        LookUpEdit1.EditValue = Nothing

        bank.Text = ""
        norek.Text = ""
        an.Text = ""
        lueBank.EditValue = Nothing
        GridControl1.DataSource = Nothing
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) 
        Dim dts As New DataTable

        dts = dataquery("select concat('PK/',substring(year(mp.createDate),3,2),'/',DATE_FORMAT(mp.createDate,'%m'),'/',if(mp.idNum<10,concat('00',mp.idNum),if(mp.idNum<100,concat('0',mp.idNum),mp.idNum))) 'Penerimaan Konsinyasi', sum((dp.qty-dr.qty)*mk.harga) 'Sub Total'
        from  mpenerimaan mp,invoice i,dtinvoice di,dtpenerimaan dp,mbarang_konsinyasi mk, mretur mr,dtretur dr
        where i.id=di.id and di.idPenerimaan=mp.id and i.id=" & getIdofIdNum1(GridView1.GetRowCellValue(0, GridView1.Columns(0))) & " and dp.id=di.idPenerimaan and dp.idBarang=mk.idBrg and dp.id=mr.idPenerimaan and mr.id=dr.id and dr.idBarang=mk.idBrg
        GROUP BY di.idPenerimaan;", conappkonsinyasi)

        'cetak catatan


        If dts.Rows.Count = 0 Then
            'jika tidak ada retur cetak payment report

            Dim x As New paymentreport
            x.Parameters("idinvoice").Value = getIdofIdNum1(GridView1.GetRowCellValue(0, GridView1.Columns(0)))
            x.Parameters("Supplier").Value = LookUpEdit1.Text
            x.Parameters("staff").Value = Module1.staff
            x.Parameters("noinvoice").Value = GridView1.GetRowCellValue(0, GridView1.Columns(0))
            x.Parameters("total").Value = repCSpr(GridView1.Columns("harga").SummaryItem.SummaryValue)
            Using prnt As UI.ReportPrintTool = New UI.ReportPrintTool(x)
                SplashScreenManager.ShowForm(GetType(WaitForm1))
                prnt.ShowPreviewDialog()
                SplashScreenManager.CloseForm()
            End Using
        Else
            Dim x As New paymentreport
            x.Parameters("idinvoice").Value = getIdofIdNum1(GridView1.GetRowCellValue(0, GridView1.Columns(0)))
            x.Parameters("Supplier").Value = LookUpEdit1.Text
            x.Parameters("staff").Value = Module1.staff
            x.Parameters("noinvoice").Value = GridView1.GetRowCellValue(0, GridView1.Columns(0))
        End If

    End Sub


End Class