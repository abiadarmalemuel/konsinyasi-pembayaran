﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class penerimaan_id
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(penerimaan_id))
        Me.cancelbut = New DevExpress.XtraEditors.SimpleButton()
        Me.okbut = New DevExpress.XtraEditors.SimpleButton()
        Me.Gridid = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.dates = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.nofaktur = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemCheckEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit()
        CType(Me.Gridid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cancelbut
        '
        Me.cancelbut.Image = CType(resources.GetObject("cancelbut.Image"), System.Drawing.Image)
        Me.cancelbut.Location = New System.Drawing.Point(251, 228)
        Me.cancelbut.Name = "cancelbut"
        Me.cancelbut.Size = New System.Drawing.Size(79, 32)
        Me.cancelbut.TabIndex = 5
        Me.cancelbut.Text = "BATAL"
        '
        'okbut
        '
        Me.okbut.Image = CType(resources.GetObject("okbut.Image"), System.Drawing.Image)
        Me.okbut.Location = New System.Drawing.Point(336, 228)
        Me.okbut.Name = "okbut"
        Me.okbut.Size = New System.Drawing.Size(75, 32)
        Me.okbut.TabIndex = 4
        Me.okbut.Text = "OK"
        '
        'Gridid
        '
        Me.Gridid.Dock = System.Windows.Forms.DockStyle.Top
        Me.Gridid.Location = New System.Drawing.Point(0, 0)
        Me.Gridid.MainView = Me.GridView1
        Me.Gridid.Name = "Gridid"
        Me.Gridid.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemCheckEdit1})
        Me.Gridid.Size = New System.Drawing.Size(412, 222)
        Me.Gridid.TabIndex = 3
        Me.Gridid.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.GridView1.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.GridView1.Appearance.Row.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GridView1.Appearance.Row.Options.UseFont = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.dates, Me.nofaktur, Me.GridColumn2})
        Me.GridView1.GridControl = Me.Gridid
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsSelection.MultiSelect = True
        Me.GridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Retur Konsinyasi"
        Me.GridColumn1.FieldName = "ids"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowEdit = False
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 1
        Me.GridColumn1.Width = 126
        '
        'dates
        '
        Me.dates.FieldName = "Date"
        Me.dates.Name = "dates"
        Me.dates.OptionsColumn.AllowEdit = False
        Me.dates.Visible = True
        Me.dates.VisibleIndex = 2
        Me.dates.Width = 110
        '
        'nofaktur
        '
        Me.nofaktur.Caption = "Faktur Supplier"
        Me.nofaktur.FieldName = "Faktur Supplier"
        Me.nofaktur.Name = "nofaktur"
        Me.nofaktur.OptionsColumn.AllowEdit = False
        Me.nofaktur.Visible = True
        Me.nofaktur.VisibleIndex = 3
        Me.nofaktur.Width = 104
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Retur"
        Me.GridColumn2.FieldName = "Retur"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.OptionsColumn.AllowEdit = False
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 4
        '
        'RepositoryItemCheckEdit1
        '
        Me.RepositoryItemCheckEdit1.AutoHeight = False
        Me.RepositoryItemCheckEdit1.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.RepositoryItemCheckEdit1.Name = "RepositoryItemCheckEdit1"
        Me.RepositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        '
        'penerimaan_id
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(412, 261)
        Me.Controls.Add(Me.cancelbut)
        Me.Controls.Add(Me.okbut)
        Me.Controls.Add(Me.Gridid)
        Me.Name = "penerimaan_id"
        Me.Text = "Penerimaan Konsinyasi"
        CType(Me.Gridid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemCheckEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents cancelbut As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents okbut As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Gridid As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemCheckEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents dates As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents nofaktur As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
End Class
