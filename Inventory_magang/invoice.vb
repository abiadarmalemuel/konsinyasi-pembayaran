﻿Imports DevExpress.XtraSplashScreen
Imports System.Globalization
Public Class invoice
    Dim x As Integer
    Dim y As Integer
    Public idarr(15) As Integer
    Public result As String
    Dim sumall
    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        'melihat data id penerimaan
        If LookUpEdit1.EditValue.ToString = String.Empty Then
            MessageBox.Show("Pilih Supplier terlebih dahulu")
        Else

            penerimaan_id.Show()
        End If
    End Sub

    Private Sub invoice_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim newCulture As CultureInfo = DirectCast(CultureInfo.CurrentCulture.Clone(), CultureInfo)
        newCulture.DateTimeFormat = (New CultureInfo("id-ID")).DateTimeFormat
        newCulture.NumberFormat = (New CultureInfo("id-ID")).NumberFormat
        System.Threading.Thread.CurrentThread.CurrentCulture = newCulture
        System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture

        Dim dt As DataTable = New DataTable
        dt = dataquery("select idSupplier 'ID',namaSupplier 'Supplier',Catatan, Telepon  from m_supplier where isKonsinyasi=1 ", conapppurchasing)
        LookUpEdit1.Properties.DataSource = dt
        LookUpEdit1.Properties.DisplayMember = "Supplier"
        LookUpEdit1.Properties.ValueMember = "ID"
        LookUpEdit1.Properties.PopulateColumns()
        LookUpEdit1.Properties.Columns("ID").Visible = False





        'no invoice
        Dim nn As Integer = dataquery("select * from invoice where month(createDate)=" & DateTime.Now.Month.ToString & " and year(createDate)=" & DateTime.Now.Year.ToString & "", conappkonsinyasi).Rows.Count
        If nn <> 0 Then
            x = Val(dataquery("select idNum from invoice where month(createDate)=" & DateTime.Now.Month.ToString & " and year(createDate)=" & DateTime.Now.Year.ToString & " order by id desc limit 1", conappkonsinyasi).Rows(0).Item("idNum").ToString)
            x = x + 1
        Else
            x = 1
        End If
        idlaritta.Text = "PI/" + DateTime.Now.Year.ToString().Substring(2, 2) + "/" + DateTime.Now.Month.ToString("#00") + "/" + getUserZeroId(x.ToString)


    End Sub

    Private Sub idpenerimaan_TextChanged(sender As Object, e As EventArgs) Handles idpenerimaan.TextChanged

        If idpenerimaan.Text <> "-" Then

            SplashScreenManager.ShowForm(GetType(WaitForm1))

            Dim dts As New DataTable

            dts = dataquery("Select  m.namaBrg 'Barang', mk.harga 'Harga',sum(d.qty) 'Qty Diterima', sum(r.qty) 'Qty Retur',sum(d.qty)-sum(r.qty) 'Qty',mk.harga*(sum(d.qty)-sum(r.qty)) 'Total'
                From db_laritta.mbarang m, dtpenerimaan d, dtretur r, mretur mr,mbarang_konsinyasi mk
                Where mk.idBrg=m.idBrg and r.id = mr.id And r.idBarang = d.idBarang And mr.idPenerimaan = d.id And d.id in " & result & " 
                And d.idBarang=m.idBrg group by m.idBrg;", conappkonsinyasi)

            If dts.Rows.Count = Nothing Then
                'jika tidak ada retur
                dts = dataquery("Select  m.namaBrg 'Barang', mk.harga 'Harga',sum(d.qty) 'Qty Diterima', 0 'Qty Retur',sum(d.qty) 'Qty',mk.harga*(sum(d.qty)) 'Total' From db_laritta.mbarang m, dtpenerimaan d, mbarang_konsinyasi mk  Where  mk.idBrg=m.idBrg and d.id in  " & result & "   And d.idBarang=m.idBrg group by m.idBrg;", conappkonsinyasi)


            End If
            GridControl1.DataSource = dts

            GridView1.PopulateColumns()
            GridView1.Columns("Harga").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            GridView1.Columns("Harga").DisplayFormat.FormatString = "n2"
            GridView1.Columns("Total").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
            GridView1.Columns("Total").DisplayFormat.FormatString = "n2"
            GridView1.Columns("Total").SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
            GridView1.Columns("Total").SummaryItem.DisplayFormat = "Rp {0:n2}"
            GridView1.Columns("Barang").OptionsColumn.AllowEdit = False
            GridView1.Columns("Harga").OptionsColumn.AllowEdit = False
            GridView1.Columns("Qty Diterima").OptionsColumn.AllowEdit = False
            GridView1.Columns("Qty Retur").OptionsColumn.AllowEdit = False
            GridView1.Columns("Qty").OptionsColumn.AllowEdit = False

            sumall = GridView1.Columns("Total").SummaryItem.SummaryValue

            SplashScreenManager.CloseForm()
        End If

    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        LookUpEdit1.EditValue = Nothing
        idpenerimaan.Text = "-"

        GridControl1.DataSource = Nothing
    End Sub

    Private Sub simpanbut_Click(sender As Object, e As EventArgs) Handles simpanbut.Click
        'penomoran idnum untuk data purchasing tandaterima
        Dim nnx As Integer = dataquery("select * from mt_tandaterima where month(createDate)=" & DateTime.Now.Month.ToString & " and year(createDate)=" & DateTime.Now.Year.ToString & "", conapppurchasing).Rows.Count
        If nnx <> 0 Then
            y = Val(dataquery("select numtandaterima from mt_tandaterima where month(createDate)=" & DateTime.Now.Month.ToString & " and year(createDate)=" & DateTime.Now.Year.ToString & " order by idtandaterima desc limit 1", conapppurchasing).Rows(0).Item("numtandaterima").ToString)
            y = y + 1
        Else
            y = 1
        End If
        Dim simpan As Boolean = True

        If simpan = True Then
            SplashScreenManager.ShowForm(GetType(WaitForm1))

            'insert master invoice
            insertall("insert into invoice(idNum,idStaff,idSupplier,subTotal,createDate,void,isPaid) values(" & Val(x) & "," & idstaff & "," & LookUpEdit1.EditValue & "," & repCSpr(GridView1.Columns("Total").SummaryItem.SummaryValue) & ",now(),0,0) ", conappkonsinyasi)
            'insert ke mt_tandaterima app_purchasing
            'insertall("insert into ")

            'insert detail invoice

            Dim num As Integer = dataquery("select id from invoice order by id desc limit 1", conappkonsinyasi).Rows(0).Item("id").ToString
            For index = 0 To 15
                If idarr(index) <> 0 Then
                    insertall("insert into dtinvoice(id,idPenerimaan) values(" & num & "," & idarr(index) & ") ", conappkonsinyasi)
                End If
            Next

            'update penerimaan konsinyasi isInvoice
            insertall("update mpenerimaan set isInvoice=1 where id in " & result & " ", conappkonsinyasi)

            'insert ke journal 
            insertall("CALL insert_journal(now(),21," & num & ",0,'penagihan konsinyasi',null," & idstaff & ",now(),'1//481//0//" & repCSpr(sumall) & "|2//476//1//" & repCSpr(sumall) & "')", conappfinance)

            SplashScreenManager.CloseForm()
            Dim rs As DialogResult = MessageBox.Show("Data Tersimpan", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            LookUpEdit1.EditValue = Nothing
            idpenerimaan.Text = "-"


            GridControl1.DataSource = Nothing
            'no invoice
            Dim nn As Integer = dataquery("select * from invoice where month(createDate)=" & DateTime.Now.Month.ToString & " and year(createDate)=" & DateTime.Now.Year.ToString & "", conappkonsinyasi).Rows.Count
            If nn <> 0 Then
                x = Val(dataquery("select idNum from invoice where month(createDate)=" & DateTime.Now.Month.ToString & " and year(createDate)=" & DateTime.Now.Year.ToString & " order by id desc limit 1", conappkonsinyasi).Rows(0).Item("idNum").ToString)
                x = x + 1
            Else
                x = 1
            End If
            idlaritta.Text = "PI/" + DateTime.Now.Year.ToString().Substring(2, 2) + "/" + DateTime.Now.Month.ToString("#00") + "/" + getUserZeroId(x.ToString)
        Else
            Dim x As DialogResult = MessageBox.Show("Data belum lengkap", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

    End Sub


End Class