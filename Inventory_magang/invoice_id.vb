﻿Imports DevExpress.XtraSplashScreen
Public Class invoice_id
    Dim dt As DataTable = New DataTable
    Public dp As DataTable = New DataTable
    Private Sub invoice_id_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SplashScreenManager.ShowForm(GetType(WaitForm1))


        dt = dataquery("SELECT concat('PI/',substring(year(m.createDate),3,2),'/',DATE_FORMAT(m.createDate,'%m'),'/',if(m.idNum<10,concat('00',m.idNum),if(m.idNum<100,concat('0',m.idNum),m.idNum))) AS ids  ,(m.createDate) 'Date', m.id 'id',m.subTotal 'harga'  from app_konsinyasi.invoice m, m_supplier where void=0 and m.isPaid=0 and m.idSupplier=m_supplier.idSupplier and m.idSupplier = " & Pembayaran.LookUpEdit1.EditValue & " ; ", conapppurchasing)
        GridView1.Columns("id").Visible = False

        GridView1.Columns("harga").Visible = False
        Gridid.DataSource = dt
        SplashScreenManager.CloseForm()

        'add ke pembayaran table
        dp.Columns.Add("id")
        dp.Columns.Add("date")
        dp.Columns.Add("harga")

    End Sub

    Private Sub okbut_Click(sender As Object, e As EventArgs) Handles okbut.Click

        For index = 0 To GridView1.RowCount - 1
            If GridView1.IsRowSelected(index) = True Then

                dp.Rows.Add(GridView1.GetRowCellValue(index, GridView1.Columns(0)), GridView1.GetRowCellValue(index, GridView1.Columns(1)), GridView1.GetRowCellValue(index, GridView1.Columns(3)))

            End If


        Next
        Pembayaran.GridControl1.DataSource = dp
        Me.Close()
    End Sub

    Private Sub cancelbut_Click(sender As Object, e As EventArgs) Handles cancelbut.Click
        Me.Close()
    End Sub
End Class