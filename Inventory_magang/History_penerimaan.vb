﻿Imports System.Globalization
Imports DevExpress.XtraSplashScreen
Imports DevExpress.XtraReports
Public Class History_penerimaan
    Private Sub Button1_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub History_penerimaan_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim newCulture As CultureInfo = DirectCast(CultureInfo.CurrentCulture.Clone(), CultureInfo)
        newCulture.DateTimeFormat = (New CultureInfo("id-ID")).DateTimeFormat
        newCulture.NumberFormat = (New CultureInfo("id-ID")).NumberFormat
        System.Threading.Thread.CurrentThread.CurrentCulture = newCulture
        System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture

        DateEdit1.EditValue = FirstDayOfMonth(DateEdit1.DateTime)
        DateEdit2.EditValue = Now.Date

        gridhistory.DataSource = queryhistory(CheckEdit1.Checked)
        GridViewhistory.PopulateColumns()
        GridViewhistory.Columns("No Invoice").OptionsColumn.AllowEdit = False


        GridViewhistory.Columns("Date").OptionsColumn.AllowEdit = False

        GridViewhistory.Columns("Paid").OptionsColumn.AllowEdit = False






    End Sub

    Private Sub gridhistory_Click(sender As Object, e As EventArgs) Handles gridhistory.Click
        'memunculkan detail penerimaan konsinyasi tiap PK

        Dim dts As New DataTable

        dts = dataquery("Select  concat('RK/',substring(year(m.createDate),3,2),'/',DATE_FORMAT(m.createDate,'%m'),'/',if(m.idNum<10,concat('00',m.idNum),if(m.idNum<100,concat('0',m.idNum),m.idNum))) 'Retur Konsinyasi', m.noFaktur 'Faktur Supplier',m.createDate 'Date'
        From mpenerimaan m,invoice i, dtinvoice d
        Where  d.idPenerimaan = m.id and i.id=d.id and i.id=" & getIdofIdNum1(GridViewhistory.GetRowCellValue(GridViewhistory.FocusedRowHandle, GridViewhistory.Columns(0))) & " ", conappkonsinyasi)

        GridControl1.DataSource = dts


    End Sub

    Private Sub CheckEdit1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckEdit1.CheckedChanged
        gridhistory.DataSource = queryhistory(CheckEdit1.Checked)
    End Sub

    Private Sub DateEdit1_EditValueChanged(sender As Object, e As EventArgs) Handles DateEdit1.EditValueChanged
        gridhistory.DataSource = queryhistory(CheckEdit1.Checked)
    End Sub

    Private Sub DateEdit2_EditValueChanged(sender As Object, e As EventArgs) Handles DateEdit2.EditValueChanged
        gridhistory.DataSource = queryhistory(CheckEdit1.Checked)
    End Sub

    Private Function queryhistory(invoice As Boolean) As DataTable
        Dim dt As DataTable
        If invoice = True Then
            dt = dataquery("select concat('PI/',substring(year(i.createDate),3,2),'/',DATE_FORMAT(i.createDate,'%m'),'/',if(i.idNum<10,concat('00',i.idNum),if(i.idNum<100,concat('0',i.idNum),i.idNum))) 'No Invoice',(i.createDate) 'Date',i.subTotal 'Sub Total',i.isPaid 'Paid' from app_konsinyasi.invoice i where DATE((i.createDate))>='" & CDate(DateEdit1.EditValue).ToString("yyyy-MM-dd") & "' and DATE((i.createDate))<='" & CDate(DateEdit2.EditValue).ToString("yyyy-MM-dd") & "' ; ", conapppurchasing)
        Else
            dt = dataquery("select concat('PI/',substring(year(i.createDate),3,2),'/',DATE_FORMAT(i.createDate,'%m'),'/',if(i.idNum<10,concat('00',i.idNum),if(i.idNum<100,concat('0',i.idNum),i.idNum))) 'No Invoice',(i.createDate) 'Date',i.subTotal 'Sub Total',i.isPaid 'Paid' from app_konsinyasi.invoice i where DATE((i.createDate))>='" & CDate(DateEdit1.EditValue).ToString("yyyy-MM-dd") & "' and DATE((i.createDate))<='" & CDate(DateEdit2.EditValue).ToString("yyyy-MM-dd") & "' and i.isPaid=0; ", conapppurchasing)
        End If
        Return dt
    End Function

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click

        Dim x As New returReport
        For index = 0 To GridViewhistory.RowCount - 1
            If GridViewhistory.IsRowSelected(index) = True Then
                If GridViewhistory.GetRowCellValue(index, GridViewhistory.Columns(4)) = True Then
                    x.Parameters("penkonsi").Value = getIdofIdNum(GridViewhistory.GetRowCellValue(index, GridViewhistory.Columns(0)))
                    x.Parameters("penerimaan").Value = GridViewhistory.GetRowCellValue(index, GridViewhistory.Columns(0))
                    x.Parameters("Supplier").Value = GridViewhistory.GetRowCellValue(index, GridViewhistory.Columns(2))
                    x.Parameters("staff").Value = Module1.staff

                    Using prnt As UI.ReportPrintTool = New UI.ReportPrintTool(x)
                        SplashScreenManager.ShowForm(GetType(WaitForm1))
                        prnt.ShowPreviewDialog()
                        SplashScreenManager.CloseForm()
                    End Using
                Else
                    Dim j As DialogResult = MessageBox.Show("Tidak dapat cetak karena tidak ada data retur", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If



            End If


        Next

    End Sub


End Class