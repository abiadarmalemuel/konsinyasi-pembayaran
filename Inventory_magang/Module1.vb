﻿Imports MySql.Data.MySqlClient
Imports System.Configuration
Module Module1

    Public info As String
    Public staff As String
    Public idstaff As String
    Public condblaritta As String = ConfigurationManager.ConnectionStrings("dblaritta").ToString
    Public conapppurchasing As String = ConfigurationManager.ConnectionStrings("apppurchasing").ToString
    Public conappkonsinyasi As String = ConfigurationManager.ConnectionStrings("appkonsinyasi").ToString
    Public conappfinance As String = ConfigurationManager.ConnectionStrings("appfinance").ToString
    Public conappbs As String = ConfigurationManager.ConnectionStrings("appbs").ToString
    Public contestappfinance As String = ConfigurationManager.ConnectionStrings("tesappfinance").ToString
    Public Function dataquery(ByVal query As String, ByVal conn As String) As DataTable
        Dim sqlDT As DataTable = New DataTable
        Try
            Dim sqlCon As New MySqlConnection(conn)
            Dim sqlDA As New MySqlDataAdapter(query, sqlCon)
            Dim sqlCB As New MySqlCommandBuilder(sqlDA)
            sqlDA.Fill(sqlDT)
        Catch ex As Exception
            MsgBox(ex.Message, Title:="Error Connection")
        End Try
        Return sqlDT
    End Function

    Public Function insertall(ByVal query As String, ByVal conn As String)

        Try

            Dim sqlCon As New MySqlConnection(conn)
            sqlCon.Open()
            Dim sqlCommand As New MySqlCommand
            sqlCommand.Connection = sqlCon
            sqlCommand.CommandText = query
            sqlCommand.ExecuteNonQuery()
            sqlCon.Close()
            Return True

        Catch ex As Exception
            Return False
            MsgBox("Error occured: Could not insert record")
        End Try

    End Function

    Public Function getIdofIdNum(ByVal text As String) As Integer
        'untuk table penerimaan
        Dim ar() As String = text.Split("/")
        Dim id As Integer

        id = Val(dataquery("select id,idNum,createDate from mpenerimaan where idNum=" & Val(ar(3)) & " and substring(year(createDate),3,2)='" & ar(1).ToString & "' and month(createDate)='" & ar(2).ToString & "' ", conappkonsinyasi).Rows(0).Item("id").ToString)
        Return id
    End Function
    Public Function getIdofIdNum1(ByVal text As String) As Integer
        'untuk tabel invoice
        Dim ar() As String = text.Split("/")
        Dim id As Integer

        id = Val(dataquery("select id,idNum,createDate from invoice where idNum=" & Val(ar(3)) & " and substring(year(createDate),3,2)='" & ar(1).ToString & "' and month(createDate)='" & ar(2).ToString & "' ", conappkonsinyasi).Rows(0).Item("id").ToString)
        Return id
    End Function

    Public Function getUserZeroId(ByVal text As Integer) As String
        Dim x As String = ""
        If text < 10 Then
            x = "00" + text.ToString
        ElseIf text > 9 And text < 100 Then
            x = "0" + text.ToString
        Else
            x = text.ToString
        End If

        Return x
    End Function
    Public Function repCSpr(ByVal m_value As String) As String 'Replace Comma Separator
        If InStr(m_value, ",") <> 0 Then
            Return Replace(m_value, ",", ".")

        Else
            Return m_value
        End If
    End Function
    Public Function FirstDayOfMonth(ByVal sourceDate As DateTime) As DateTime
        Return New DateTime(Now.Year, Now.Month, 1)
    End Function





End Module
