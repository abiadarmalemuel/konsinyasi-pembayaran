﻿Imports DevExpress.XtraSplashScreen
Public Class penerimaan_id


    Private Sub penerimaan_id_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SplashScreenManager.ShowForm(GetType(WaitForm1))
        Dim dt As DataTable = New DataTable
        dt = dataquery("SELECT concat('RK/',substring(year(m.createDate),3,2),'/',DATE_FORMAT(m.createDate,'%m'),'/',if(m.idNum<10,concat('00',m.idNum),if(m.idNum<100,concat('0',m.idNum),m.idNum))) AS ids  ,(m.createDate) 'Date',noFaktur 'Faktur Supplier',id,m.isRetur 'Retur'  from app_konsinyasi.mpenerimaan m, m_supplier where  m.isInvoice=0 and m.idSupplier=m_supplier.idSupplier and m.idSupplier = " & invoice.LookUpEdit1.EditValue & " ; ", conapppurchasing)
        Gridid.DataSource = dt
        SplashScreenManager.CloseForm()

    End Sub

    Private Sub okbut_Click(sender As Object, e As EventArgs) Handles okbut.Click
        Dim idpencom As String = ""
        Dim counter As Integer = 0
        Array.Clear(invoice.idarr, 0, invoice.idarr.Length)
        For index = 0 To GridView1.RowCount - 1
            If GridView1.IsRowSelected(index) = True Then
                invoice.idarr(counter) = getIdofIdNum(GridView1.GetRowCellValue(index, GridView1.Columns(0)))
                If counter = 0 Then
                    idpencom = GridView1.GetRowCellValue(index, GridView1.Columns(0))
                    invoice.result = "(" + invoice.idarr(counter).ToString
                    counter = counter + 1

                Else
                    idpencom = idpencom + ", " + GridView1.GetRowCellValue(index, GridView1.Columns(0))
                    invoice.result = invoice.result & " , " & invoice.idarr(counter).ToString
                    counter = counter + 1
                End If


            End If

        Next
        invoice.result = invoice.result & ")"
        invoice.idpenerimaan.Text = idpencom

        Me.Close()
    End Sub

    Private Sub cancelbut_Click(sender As Object, e As EventArgs) Handles cancelbut.Click
        Me.Close()
    End Sub
End Class