﻿Imports DevExpress.XtraEditors.Repository
Imports System.Globalization
Imports DevExpress.XtraSplashScreen
Public Class master_konsinyasi
    Dim dtb As DataTable = New DataTable
    Dim dts As DataTable = New DataTable
    Public Sub master_konsinyasi_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim newCulture As CultureInfo = DirectCast(CultureInfo.CurrentCulture.Clone(), CultureInfo)
        newCulture.DateTimeFormat = (New CultureInfo("id-ID")).DateTimeFormat
        newCulture.NumberFormat = (New CultureInfo("id-ID")).NumberFormat
        System.Threading.Thread.CurrentThread.CurrentCulture = newCulture
        System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture
        'data supplier
        Dim dt As DataTable = New DataTable
        dt = dataquery("select idSupplier 'ID',namaSupplier 'Supplier',Catatan, Telepon,if(isKonsinyasi=False,'Tidak','Ya') 'Konsinyasi'  from m_supplier ", conapppurchasing)
        LookUpEdit1.Properties.DataSource = dt
        LookUpEdit1.Properties.DisplayMember = "Supplier"
        LookUpEdit1.Properties.ValueMember = "ID"
        LookUpEdit1.Properties.PopulateColumns()
        LookUpEdit1.Properties.Columns("ID").Visible = False

        'data barang



        dts = dataquery("select idBrg 'id',FALSE AS Pilih,namaBrg 'Barang',mb.namaKategori2 'Kategori' from mbarang m, mkategoribrg2 mb where m.idKategori2=mb.idKategori2;", condblaritta)


        gridbarang.DataSource = dts



        GridViewbarang.Columns("id").Visible = False
        GridViewbarang.Columns("Barang").OptionsColumn.AllowEdit = False
        GridViewbarang.Columns("Kategori").OptionsColumn.AllowEdit = False





        'grid harga

        dtb.Columns.Add("id")
        dtb.Columns.Add("Barang")
        dtb.Columns.Add("Harga")
        gridharga.DataSource = dtb

        GridViewharga.Columns("id").Visible = False
        GridViewharga.Columns("Barang").OptionsColumn.AllowEdit = False
        GridViewharga.Columns("Harga").DisplayFormat.FormatString = "Rp {0:n2}"
        GridViewharga.Columns("Harga").DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric

        GridViewharga.Columns("Harga").AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        GridViewharga.Columns("Barang").AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center
        GridViewharga.Columns("Harga").AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far




    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        'tambah barang konsinyasi
        Dim ctr As Integer = 0
        Dim valid As Boolean = True

        For index = 0 To GridViewbarang.RowCount - 1
            If GridViewbarang.GetRowCellValue(index, GridViewbarang.Columns(1)) = 1 Then
                If GridViewharga.RowCount - 1 <> -1 Then
                    For x = 0 To GridViewharga.RowCount - 1
                        If GridViewbarang.GetRowCellValue(index, GridViewbarang.Columns(0)) = GridViewharga.GetRowCellValue(index, GridViewharga.Columns(0)) Then
                            valid = False

                        End If
                    Next
                End If

                If valid = True Then
                    dtb.Rows.Add(GridViewbarang.GetRowCellValue(index, GridViewbarang.Columns(0)), GridViewbarang.GetRowCellValue(index, GridViewbarang.Columns(2)))

                End If
                GridViewbarang.SetRowCellValue(index, GridViewbarang.Columns(1), 0)
            End If
        Next
        If valid = False Then
            MessageBox.Show("Tidak dapat menambah barang yang sama", "Input tidak sesuai")
        End If
    End Sub

    Private Sub LookUpEdit1_EditValueChanged(sender As Object, e As EventArgs) Handles LookUpEdit1.EditValueChanged
        'list barang supplier yang sudah ada
        If LookUpEdit1.EditValue <> Nothing Then
            Dim dtx As DataTable = New DataTable
            dtx = dataquery("select m.namaBrg 'Barang',nm.namaKategori2 'Kategori' from mbarang m,mkategoribrg2 nm, app_konsinyasi.mbarang_konsinyasi mk where m.idBrg =mk.idBrg and nm.idKategori2=m.idKategori2 and mk.idSupplier=" & LookUpEdit1.EditValue & "", condblaritta)
            gridsupplier.DataSource = dtx
            GridViewsupplier.Columns("Barang").OptionsColumn.AllowEdit = False
            GridViewsupplier.Columns("Kategori").OptionsColumn.AllowEdit = False
        End If

    End Sub

    Private Sub simpanbut_Click(sender As Object, e As EventArgs) Handles simpanbut.Click
        Dim simpan As Boolean = False
        If LookUpEdit1.EditValue <> Nothing Then

            For index = 0 To GridViewharga.RowCount - 1

                If GridViewharga.GetRowCellValue(index, GridViewharga.Columns(2)).ToString <> "" Then
                    simpan = True

                End If
            Next

        End If

        If simpan = True Then
            SplashScreenManager.ShowForm(GetType(WaitForm1))
            'update msupplier from iskonsinyasi 0 to iskonsinyasi 1
            insertall("update m_supplier set isKonsinyasi=1 where idSupplier =  " & LookUpEdit1.EditValue & " ", conapppurchasing)
            'insert to mbarang_konsinyasi
            Dim rw As Integer = GridViewharga.RowCount - 1
            For index = 0 To rw
                insertall("insert into mbarang_konsinyasi values(" & GridViewharga.GetRowCellValue(index, GridViewharga.Columns(0)) & "," & LookUpEdit1.EditValue & ",'" & repCSpr(GridViewharga.GetRowCellValue(index, GridViewharga.Columns(2))) & "')", conappkonsinyasi)
            Next
            SplashScreenManager.CloseForm()
            Dim rs As DialogResult = MessageBox.Show("Data Tersimpan", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            'clear data
            LookUpEdit1.EditValue = Nothing
            For index = 0 To GridViewharga.RowCount - 1
                GridViewharga.DeleteRow(index)
            Next
            For index = 0 To GridViewsupplier.RowCount - 1
                GridViewsupplier.DeleteRow(index)
            Next

        Else
            Dim x As DialogResult = MessageBox.Show("Data belum lengkap", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If



    End Sub

    Private Sub cancelbut_Click(sender As Object, e As EventArgs) Handles cancelbut.Click
        'clear data
        LookUpEdit1.EditValue = Nothing
        For index = 0 To GridViewharga.RowCount - 1
            GridViewharga.DeleteRow(index)
        Next
        For index = 0 To GridViewsupplier.RowCount - 1
            GridViewsupplier.DeleteRow(index)
        Next

    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        LookUpEdit1.EditValue = Nothing
        For index = 0 To GridViewharga.RowCount - 1
            GridViewharga.DeleteRow(index)
        Next
    End Sub
End Class